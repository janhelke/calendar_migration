<?php
declare(strict_types=1);

if (!defined('TYPO3')) {
    die('Access denied.');
}

(static function () {
    $GLOBALS['TCA']['tx_calendar_deviation']['columns']['tx_cal_event_deviation_uid'] = [
        'exclude' => 0,
        'label' => 'LLL:EXT:calendar_migration/Resources/Private/Language/locallang_db.xlf:tx_calendar_deviation.tx_cal_event_deviation_uid',
        'config' => [
            'type' => 'passthrough',

        ],
    ];
})();

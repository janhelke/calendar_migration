<?php
declare(strict_types=1);

if (!defined('TYPO3')) {
    die('Access denied.');
}

(static function () {
    $GLOBALS['TCA']['tx_calendar_exception_group']['columns']['tx_cal_exception_event_group_uid'] = [
        'exclude' => 0,
        'label' => 'LLL:EXT:calendar_migration/Resources/Private/Language/locallang_db.xlf:tx_calendar_exception.tx_cal_exception_event_group_uid',
        'config' => [
            'type' => 'passthrough',

        ],
    ];
})();

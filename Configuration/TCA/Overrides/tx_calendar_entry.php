<?php
declare(strict_types=1);

if (!defined('TYPO3')) {
    die('Access denied.');
}

(static function () {
    $GLOBALS['TCA']['tx_calendar_entry']['columns']['tx_cal_event_uid'] = [
        'exclude' => 0,
        'label' => 'LLL:EXT:calendar_migration/Resources/Private/Language/locallang_db.xlf:tx_calendar_entry.tx_cal_event_uid',
        'config' => [
            'type' => 'passthrough',

        ],
    ];
})();

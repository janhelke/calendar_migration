# TYPO3 Extension calendar_migration
The target of this extension is to provide a smooth transition from ext:cal ti the newly developed calendar_framework

## Migrating via the install tool
The most obvious possibility to migrate entries will be the upgrade wizard within the Admin Tools => Upgrade Module in 
TYPO3 9LTS respectively in the install tool in TYPO8 LTS. You will find an additional Upgrade Wizard named "Migrate 
entries from ext:cal to calendar_foundation".

## Migrating via the shell
The Upgrade Module is a valid way, if you have only a couple of entries (e.g. 200 or so). If you have huge databases or 
are a speedster, this extension offers also a CLI script "calendar:migrate". Using this script processes the entries 
much faster.

## Lacking migrations
It is absolute likely, that not all features of ext:cal will be migrated. Mostly because the calendar_framework is 
lacking either needed functionality or this features were just dropped to ensure a minimal viable product. But during 
the migration no database entry of ext:cal is touched. In fact every migrated entry from ext:cal to calendar_framework 
will be enriched with the uid of the original ext:cal entry. So it should be easy for the developer in need to create 
an own extension to continue the work of this extension.

## Missing support for TYPO3 10
It is intended, that despite from all other calendar_framework extensions, this will not installable via composer in a
TYPO3 10 environment. This is due to the fact, that ext:cal will never be compatible with TYPO3 10. So the intention is,
that you do your cal to calendar_framework migration either in TYPO3 8 LTS or in TYPO3 9 LTS. I am pretty sure, that are
the most common use cases. If for some reasons your are not able to use TYPO3 8 or 9, you can do the migration also with
version 10. Just download the extension as a zip package, extract it to your extension folder and install it in your 
preferred way (extension manager or CLI). You'll have access to the upgrade wizards.

BUT keep in mind, the extension is not tested with TYPO3 10. So always be prepared for some strange side effects.

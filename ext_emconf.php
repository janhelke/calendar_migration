<?php

$EM_CONF['calendar_migration'] = [
    'title' => 'Calendar Migration',
    'description' => 'This extension aims to migrate all content from ext:cal to the new calendar_framework.',
    'category' => 'plugin',
    'version' => '1.0.0',
    'state' => 'stable',
    'author' => 'Jan Helke',
    'author_email' => 'calendar@typo3.helke.de',
    'constraints' => [
        'depends' => [
            'calendar_api' => '1.0 - 1.999'
        ],
    ]
];

<?php
declare(strict_types=1);

namespace JanHelke\CalendarMigration\Tests\Functional;

use JanHelke\CalendarMigration\Service\MigrateCalToCalendarFoundationService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\TestingFramework\Core\Functional\FunctionalTestCase;

/**
 * Class MigrateCalEntriesServiceFunctionalTest
 */
class MigrateCalEntriesServiceTest extends FunctionalTestCase
{
    /**
     * @var string[]
     */
    protected $testExtensionsToLoad = [
        'typo3conf/ext/calendar_foundation',
        'typo3conf/ext/calendar_migration',
    ];

    public function testFullMigration(): void
    {
        $this->importCSVDataSet(GeneralUtility::getFileAbsFileName('EXT:calendar_migration/Tests/Functional/Fixtures/pre_migration_cal_events.csv'));
        (new MigrateCalToCalendarFoundationService())->processMigration();
        $this->assertCSVDataSet('EXT:calendar_migration/Tests/Functional/Fixtures/post_migration_calendar_entries.csv');
    }
}

<?php
declare(strict_types=1);

namespace JanHelke\CalendarMigration\Tests\Unit;

use JanHelke\CalendarMigration\Service\MigrateCalToCalendarFoundationService;
use Prophecy\PhpUnit\ProphecyTrait;
use TYPO3\CMS\Core\Localization\LanguageServiceFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * MigrateCalCalendarsToCalendarFoundationServiceTest
 */
class MigrateCalCalendarsToCalendarFoundationServiceTest extends UnitTestCase
{
    use ProphecyTrait;

    /**
     * @return array
     */
    public function migrateCalendarDataProvider(): array
    {
        /**
        $exampleCalCalendarTypo3Fields = [
            'uid' => 1,
            'pid' => 1,
            'tstamp' => 1563720870,
            'crdate' => 1375547974,
            'cruser_id' => 104,
            'deleted' => 0,
            'hidden' => 0,
            'starttime' => 0,
            'endtime' => 0,

            't3ver_oid' => 0,
            't3ver_id' => 0,
            't3ver_wsid' => 0,
            't3ver_label' => 'BLOB',
            't3ver_state' => 0,
            't3ver_stage' => 0,
            't3ver_count' => 0,
            't3ver_tstamp' => 0,
            't3_origuid' => 0,
            'sys_language_uid' => 0,
            'l18n_parent' => 0,
            'l18n_diffsource' => 'BLOB',
            'no_auto_pb' => 0,
            't3ver_move_id' => 0,
            'l10n_state' => null

        ];

        $exampleCalCalendar = [
            'title' => 'Foo',
            'activate_fnb' => 0,
            'fnb_user_cnt' => 0,
            'type' => 2,
            'ext_url' => 'BLOB',
            'ext_url_notes' => 'BLOB',
            'ics_file' => 'BLOB',
            'refresh' => 60,
            'md5' => 'BLOB',
            'schedulerId' => 0,
        ];

        $exampleCalendarCalendar = [
            'title' => '',
            'activate_free_and_busy' => 0,
            'free_and_busy_users_and_groups' => 0,
            'type' => 0,
            'ical_url' => '',
            'ical_file' => 0,
            'refresh_interval' => 0,
            'md5' => '',
            'scheduler_id' => 0,
        ];
         **/
        return [
            'normal calendar' => [
                'calCalendar' => [
                    'uid' => 23,
                    'pid' => 1,
                    'title' => 'Foo',
                    'activate_fnb' => 0,
                    'fnb_user_cnt' => 0,
                    'type' => 0,
                    'ext_url' => '',
                    'ext_url_notes' => '',
                    'ics_file' => '',
                    'refresh' => 60,
                    'md5' => '',
                    'schedulerId' => 0,
                ],
                'expectedCalendar' => [
                    'pid' => 1,
                    'title' => 'Foo',
                    'activate_free_and_busy' => 0,
                    'free_and_busy_users_and_groups' => 0,
                    'type' => 'calendar',
                    'ical_url' => '',
                    'ical_file' => 0,
                    'refresh_interval' => 0,
                    'md5' => '',
                    'scheduler_id' => 0,
                    'tx_cal_calendar_uid' => 23
                ]
            ],
            'normal calendar with free and busy' => [
                'calCalendar' => [
                    'uid' => 23,
                    'pid' => 1,
                    'title' => 'Foo',
                    'activate_fnb' => 1,
                    'fnb_user_cnt' => 0,
                    'type' => 0,
                    'ext_url' => '',
                    'ext_url_notes' => '',
                    'ics_file' => '',
                    'refresh' => 60,
                    'md5' => '',
                    'schedulerId' => 0,
                ],
                'expectedCalendar' => [
                    'pid' => 1,
                    'title' => 'Foo',
                    'activate_free_and_busy' => 1,
                    'free_and_busy_users_and_groups' => 0,
                    'type' => 'calendar',
                    'ical_url' => '',
                    'ical_file' => 0,
                    'refresh_interval' => 0,
                    'md5' => '',
                    'scheduler_id' => 0,
                    'tx_cal_calendar_uid' => 23
                ]
            ],
            /**
            'normal calendar with free and busy with user and groups' => [
                'calCalendar' => [
            'uid' => 23,

            'title' => 'Foo',
                    'activate_fnb' => 1,
                    'fnb_user_cnt' => 0,
                    'type' => 0,
                    'ext_url' => '',
                    'ext_url_notes' => '',
                    'ics_file' => '',
                    'refresh' => 60,
                    'md5' => '',
                    'schedulerId' => 0,
                ],
                'expectedCalendar' => [
                    'title' => 'Foo',
                    'activate_free_and_busy' => 1,
                    'free_and_busy_users_and_groups' => 0,
                    'type' => 0,
                    'ical_url' => '',
                    'ical_file' => 0,
                    'refresh_interval' => 0,
                    'md5' => '',
                    'scheduler_id' => 0,
                ]
            ],

            'ical feed calendar' => [
                'calCalendar' => [
            'uid' => 23,

                ],
                'expectedCalendar' => [

                ]

            ],
            'ical file calendar' => [
                'calCalendar' => [
            'uid' => 23,

                ],
                'expectedCalendar' => [

                ]
            ]
             */
        ];
    }

    /**
     * @param array $calCalendar
     * @param array $expectedCalendarCalendar
     * @dataProvider migrateCalendarDataProvider
     */
    public function testMigrateCalendar(
        array $calCalendar,
        array $expectedCalendarCalendar
    ): void {
        $languageServiceFactoryProphecy = $this->prophesize(LanguageServiceFactory::class);
        GeneralUtility::addInstance(LanguageServiceFactory::class, $languageServiceFactoryProphecy->reveal());

        $this->resetSingletonInstances = true;
        $updateWizard = new MigrateCalToCalendarFoundationService();
        self::assertEquals(
            $expectedCalendarCalendar,
            $updateWizard->migrateCalendar($calCalendar)
        );
        GeneralUtility::purgeInstances();
    }
}

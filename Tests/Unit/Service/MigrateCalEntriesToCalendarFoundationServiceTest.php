<?php
declare(strict_types=1);

namespace JanHelke\CalendarMigration\Tests\Unit;

use JanHelke\CalendarFoundation\Service\EventService;
use JanHelke\CalendarMigration\Service\MigrateCalToCalendarFoundationService;
use Prophecy\PhpUnit\ProphecyTrait;
use TYPO3\CMS\Core\Localization\LanguageServiceFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * MigrateCalEntriesToCalendarFoundationServiceTest
 */
class MigrateCalEntriesToCalendarFoundationServiceTest extends UnitTestCase
{
    use ProphecyTrait;

    /**
     * @return array
     */
    public function migrateEntryDataProvider(): array
    {
        /**
        $exampleCalEntry = [
            'start_date' => '',
            'end_date' => '',
            'start_time' => '',
            'end_time' => '',
            'allday' => '',
            'timezone' => '',
            'title' => '',
            'calendar_id' => '',
            'category_id' => '',
            'organizer' => '',
            'organizer_id' => '',
            'organizer_pid' => '',
            'organizer_link' => '',
            'location' => '',
            'location_id' => '',
            'location_pid' => '',
            'location_link' => '',
            'teaser' => '',
            'description' => '',
            'freq' => '',
            'until' => '',
            'cnt' => '',
            'byday' => '',
            'bymonthday' => '',
            'bymonth' => '',
            'intrval' => '',
            'rdate' => '',
            'rdate_type' => '',
            'deviation' => '',
            'monitor_cnt' => '',
            'exception_cnt' => '',
            'fe_cruser_id' => '',
            'fe_crgroup_id' => '',
            'shared_user_cnt' => '',
            'type' => '',
            'page' => '',
            'ext_url' => '',
            'isTemp' => '',
            'icsUid' => '',
            'image' => '',
            'attachment' => '',
            'ref_event_id' => '',
            'send_invitation' => '',
            'attendee' => '',
            'status' => '',
            'priority' => '',
            'completed' => '',
        ];
         */
        $expectedCalendarEntry = [
            'pid' => 5,
            'tstamp' => 0,
            'crdate' => 0,
            'cruser_id' => 0,
            'deleted' => 0,
            'hidden' => 0,
            'starttime' => '',
            'endtime' => '',
            'sys_language_uid' => 0,
            'l18n_parent' => 0,
            'l10n_state' => '',
            'l18n_diffsource' => '',
            'start' => '2019-07-16 10:23:00',
            'end' => '2019-07-16 15:42:00',
            'title' => 'title',
            'teaser' => 'teaser',
            'description' => 'description',
            'tx_cal_event_uid' => 23
        ];

        return [
            'normal entry' => [
                'calEntry' => [
                    'uid' => 23,
                    'pid' => 5,
                    'start_date' => '20190716',
                    'end_date' => '20190716',
                    'start_time' => '37380',
                    'end_time' => '56520',
                    'timezone' => 'UTC',
                    'title' => 'title',
                    'teaser' => 'teaser',
                    'description' => 'description',
                ],
                $expectedCalendarEntry,
                'expectedRecurrenceEntry' => []
            ],
            'daily entry' => [
                'calEntry' => [
                    'uid' => 23,
                    'pid' => 5,
                    'start_date' => '20190716',
                    'end_date' => '20190716',
                    'start_time' => '37380',
                    'end_time' => '56520',
                    'timezone' => 'UTC',
                    'title' => 'title',
                    'teaser' => 'teaser',
                    'description' => 'description',
                    'freq' => 'day',
                    'intrval' => 1
                ],
                $expectedCalendarEntry,
                'expectedRecurrenceEntry' => [
                    [
                        'pid' => 5,
                        'tstamp' => 0,
                        'crdate' => 0,
                        'cruser_id' => 0,
                        'frequency' => EventService::DAILY_RECURRENCE,
                        'interval' => 1,
                        'by_day_of_week' => 0,
                        'by_day_index' => 0,
                        'by_day_day' => 0,
                        'by_day_of_month' => 0,
                        'until_date' => null,
                        'until_recurrence_amount' => 0
                    ]
                ]
            ],
            'daily entry with until date' => [
                'calEntry' => [
                    'uid' => 23,
                    'pid' => 5,
                    'start_date' => '20190716',
                    'end_date' => '20190716',
                    'start_time' => '37380',
                    'end_time' => '56520',
                    'timezone' => 'UTC',
                    'title' => 'title',
                    'teaser' => 'teaser',
                    'description' => 'description',
                    'freq' => 'day',
                    'intrval' => 2,
                    'until' => 20_190_725,
                ],
                $expectedCalendarEntry,
                'expectedRecurrenceEntry' => [
                    [
                        'pid' => 5,
                        'tstamp' => 0,
                        'crdate' => 0,
                        'cruser_id' => 0,
                        'frequency' => EventService::DAILY_RECURRENCE,
                        'interval' => 2,
                        'by_day_of_week' => 0,
                        'by_day_index' => 0,
                        'by_day_day' => 0,
                        'by_day_of_month' => 0,
                        'until_date' => '2019-07-25 00:00:00',
                        'until_recurrence_amount' => 0
                    ]
                ]
            ],
            'daily entry with until amount' => [
                'calEntry' => [
                    'uid' => 23,
                    'pid' => 5,
                    'start_date' => '20190716',
                    'end_date' => '20190716',
                    'start_time' => '37380',
                    'end_time' => '56520',
                    'timezone' => 'UTC',
                    'title' => 'title',
                    'teaser' => 'teaser',
                    'description' => 'description',
                    'freq' => 'day',
                    'intrval' => 3,
                    'cnt' => 23,
                ],
                $expectedCalendarEntry,
                'expectedRecurrenceEntry' => [
                    [
                        'pid' => 5,
                        'tstamp' => 0,
                        'crdate' => 0,
                        'cruser_id' => 0,
                        'frequency' => EventService::DAILY_RECURRENCE,
                        'interval' => 3,
                        'by_day_of_week' => 0,
                        'by_day_index' => 0,
                        'by_day_day' => 0,
                        'by_day_of_month' => 0,
                        'until_date' => null,
                        'until_recurrence_amount' => 23
                    ]
                ]
            ],
            'weekly entry' => [
                'calEntry' => [
                    'uid' => 23,
                    'pid' => 5,
                    'start_date' => '20190716',
                    'end_date' => '20190716',
                    'start_time' => '37380',
                    'end_time' => '56520',
                    'timezone' => 'UTC',
                    'title' => 'title',
                    'teaser' => 'teaser',
                    'description' => 'description',
                    'freq' => 'week',
                    'intrval' => 1,
                    'byday' => 'tu,fr'
                ],
                $expectedCalendarEntry,
                'expectedRecurrenceEntry' => [
                    [
                        'pid' => 5,
                        'tstamp' => 0,
                        'crdate' => 0,
                        'cruser_id' => 0,
                        'frequency' => EventService::WEEKLY_RECURRENCE,
                        'interval' => 1,
                        'by_day_of_week' => 36,
                        'by_day_index' => 0,
                        'by_day_day' => 0,
                        'by_day_of_month' => 0,
                        'until_date' => null,
                        'until_recurrence_amount' => 0
                    ]
                ]
            ],
            'monthly entry by_day_index by_day_day' => [
                'calEntry' => [
                    'uid' => 23,
                    'pid' => 5,
                    'start_date' => '20190716',
                    'end_date' => '20190716',
                    'start_time' => '37380',
                    'end_time' => '56520',
                    'timezone' => 'UTC',
                    'title' => 'title',
                    'teaser' => 'teaser',
                    'description' => 'description',
                    'freq' => 'month',
                    'intrval' => 1,
                    'byday' => '2sa'
                ],
                $expectedCalendarEntry,
                'expectedRecurrenceEntry' => [
                    [
                        'pid' => 5,
                        'tstamp' => 0,
                        'crdate' => 0,
                        'cruser_id' => 0,
                        'frequency' => EventService::MONTHLY_RECURRENCE,
                        'interval' => 1,
                        'by_day_of_week' => 0,
                        'by_day_index' => 2,
                        'by_day_day' => 7,
                        'by_day_of_month' => 0,
                        'until_date' => null,
                        'until_recurrence_amount' => 0
                    ]
                ]
            ],
            'monthly entry many by_day_index by_day_day' => [
                'calEntry' => [
                    'uid' => 23,
                    'pid' => 5,
                    'start_date' => '20190716',
                    'end_date' => '20190716',
                    'start_time' => '37380',
                    'end_time' => '56520',
                    'timezone' => 'UTC',
                    'title' => 'title',
                    'teaser' => 'teaser',
                    'description' => 'description',
                    'freq' => 'month',
                    'intrval' => 1,
                    'byday' => '2sa, -3fr, 5mo'
                ],
                $expectedCalendarEntry,
                'expectedRecurrenceEntry' => [
                    [
                        'pid' => 5,
                        'tstamp' => 0,
                        'crdate' => 0,
                        'cruser_id' => 0,
                        'frequency' => EventService::MONTHLY_RECURRENCE,
                        'interval' => 1,
                        'by_day_of_week' => 0,
                        'by_day_index' => 2,
                        'by_day_day' => 7,
                        'by_day_of_month' => 0,
                        'until_date' => null,
                        'until_recurrence_amount' => 0
                    ],
                    [
                        'pid' => 5,
                        'tstamp' => 0,
                        'crdate' => 0,
                        'cruser_id' => 0,
                        'frequency' => EventService::MONTHLY_RECURRENCE,
                        'interval' => 1,
                        'by_day_of_week' => 0,
                        'by_day_index' => -3,
                        'by_day_day' => 6,
                        'by_day_of_month' => 0,
                        'until_date' => null,
                        'until_recurrence_amount' => 0
                    ],
                    [
                        'pid' => 5,
                        'tstamp' => 0,
                        'crdate' => 0,
                        'cruser_id' => 0,
                        'frequency' => EventService::MONTHLY_RECURRENCE,
                        'interval' => 1,
                        'by_day_of_week' => 0,
                        'by_day_index' => 5,
                        'by_day_day' => 2,
                        'by_day_of_month' => 0,
                        'until_date' => null,
                        'until_recurrence_amount' => 0
                    ]
                ]
            ],
            'monthly entry bymonthday' => [
                'calEntry' => [
                    'uid' => 23,
                    'pid' => 5,
                    'start_date' => '20190716',
                    'end_date' => '20190716',
                    'start_time' => '37380',
                    'end_time' => '56520',
                    'timezone' => 'UTC',
                    'title' => 'title',
                    'teaser' => 'teaser',
                    'description' => 'description',
                    'freq' => 'month',
                    'intrval' => 1,
                    'bymonthday' => '3,11'
                ],
                $expectedCalendarEntry,
                'expectedRecurrenceEntry' => [
                    [
                        'pid' => 5,
                        'tstamp' => 0,
                        'crdate' => 0,
                        'cruser_id' => 0,
                        'frequency' => EventService::MONTHLY_RECURRENCE,
                        'interval' => 1,
                        'by_day_of_week' => 0,
                        'by_day_index' => 0,
                        'by_day_day' => 0,
                        'by_day_of_month' => 1028,
                        'until_date' => null,
                        'until_recurrence_amount' => 0
                    ]
                ]
            ]
        ];
    }

    /**
     * @param array $calEntry
     * @param array $expectedCalendarEntry
     * @param array $expectedRecurrenceEntry
     * @dataProvider migrateEntryDataProvider
     */
    public function testMigrateEntry(
        array $calEntry,
        array $expectedCalendarEntry,
        array $expectedRecurrenceEntry
    ): void {
        $languageServiceFactoryProphecy = $this->prophesize(LanguageServiceFactory::class);
        GeneralUtility::addInstance(LanguageServiceFactory::class, $languageServiceFactoryProphecy->reveal());

        $this->resetSingletonInstances = true;
        $updateWizard = new MigrateCalToCalendarFoundationService();
        $updateWizard->setPidMatching(
            [
                5 => 5
            ]
        );
        self::assertEquals(
            [
                'tx_calendar_entry' => $expectedCalendarEntry,
                'tx_calendar_recurrence' => $expectedRecurrenceEntry
            ],
            $updateWizard->migrateEntry($calEntry)
        );
        GeneralUtility::purgeInstances();
    }
}

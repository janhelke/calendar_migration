<?php
declare(strict_types=1);

namespace JanHelke\CalendarMigration\Domain\Repository;

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class CalRepository
 */
class CalRepository
{
    /**
     * @return mixed[]
     */
    public function findAllEntries(): array
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_cal_event');
        return $queryBuilder
            ->select('*')
            ->from('tx_cal_event')
            ->execute()
            ->fetchAll();
    }

    /**
     * @return array
     */
    public function findAllPids(): array
    {
        $pids = [];
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_cal_event');
        $selectPids = $queryBuilder
            ->select('pid')
            ->from('tx_cal_event')
            ->groupBy('pid')
            ->execute()
            ->fetchAll();
        foreach ($selectPids as $pid) {
            $pids[] = $pid['pid'];
        }

        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_cal_calendar');
        $selectPids = $queryBuilder
            ->select('pid')
            ->from('tx_cal_calendar')
            ->groupBy('pid')
            ->execute()
            ->fetchAll();
        foreach ($selectPids as $pid) {
            $pids[] = $pid['pid'];
        }

        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_cal_event_deviation');
        $selectPids = $queryBuilder
            ->select('pid')
            ->from('tx_cal_event_deviation')
            ->groupBy('pid')
            ->execute()
            ->fetchAll();
        foreach ($selectPids as $pid) {
            $pids[] = $pid['pid'];
        }

        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_cal_exception_event');
        $selectPids = $queryBuilder
            ->select('pid')
            ->from('tx_cal_exception_event')
            ->groupBy('pid')
            ->execute()
            ->fetchAll();
        foreach ($selectPids as $pid) {
            $pids[] = $pid['pid'];
        }

        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_cal_exception_event_group');
        $selectPids = $queryBuilder
            ->select('pid')
            ->from('tx_cal_exception_event_group')
            ->groupBy('pid')
            ->execute()
            ->fetchAll();
        foreach ($selectPids as $pid) {
            $pids[] = $pid['pid'];
        }

        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_cal_exception_event_group');
        $selectPids = $queryBuilder
            ->select('pid')
            ->from('tx_cal_exception_event_group')
            ->groupBy('pid')
            ->execute()
            ->fetchAll();
        foreach ($selectPids as $pid) {
            $pids[] = $pid['pid'];
        }

        return array_unique($pids);
    }

    /**
     * @return mixed[]
     */
    public function findAllCalendars(): array
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_cal_calendar');
        return $queryBuilder
            ->select('*')
            ->from('tx_cal_calendar')
            ->execute()
            ->fetchAll();
    }

    /**
     * @param int $eventUid
     * @return array
     */
    public function findDeviationsByEventUid(int $eventUid): array
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_cal_event_deviation');
        return $queryBuilder
            ->select('*')
            ->from('tx_cal_event_deviation')
            ->where(
                $queryBuilder->expr()->eq(
                    'parentid',
                    $queryBuilder->createNamedParameter($eventUid, \PDO::PARAM_INT)
                )
            )
            ->execute()
            ->fetchAll();
    }

    /**
     * @param int $eventUid
     * @return array
     */
    public function findExceptionsByEventUid(int $eventUid): array
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_cal_exception_event');
        return $queryBuilder
            ->select('*')
            ->from('tx_cal_exception_event', 'exception')
            ->join('exception', 'tx_cal_exception_event_mm', 'mm', 'mm.uid_local = exception.uid')
            ->where(
                $queryBuilder->expr()->eq(
                    'uid_foreign',
                    $queryBuilder->createNamedParameter($eventUid, \PDO::PARAM_INT)
                )
            )
            ->execute()
            ->fetchAll();
    }

    /**
     * @return array
     */
    public function findAllExceptions(): array
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_cal_exception_event');
        return $queryBuilder
            ->select('*')
            ->from('tx_cal_exception_event')
            ->execute()
            ->fetchAll();
    }

    /**
     * @return array
     */
    public function findAllExceptionGroups(): array
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_cal_exception_event_group');
        return $queryBuilder
            ->select('*')
            ->from('tx_cal_exception_event_group')
            ->execute()
            ->fetchAll();
    }

    /**
     * @param int $exceptionGroupUid
     * @return array
     */
    public function findAllExceptionsInGroup(int $exceptionGroupUid): array
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_cal_exception_event_group_mm');
        return $queryBuilder
            ->select('*')
            ->from('tx_cal_exception_event_group_mm')
            ->where(
                $queryBuilder->expr()->eq(
                    'uid_local',
                    $queryBuilder->createNamedParameter($exceptionGroupUid, \PDO::PARAM_INT)
                )
            )
            ->execute()
            ->fetchAll();
    }
}

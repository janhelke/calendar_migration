<?php
declare(strict_types=1);

namespace JanHelke\CalendarMigration\Command;

use JanHelke\CalendarMigration\Service\MigrateCalToCalendarFoundationService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class MigrateCalEntriesToCalendarCommand
 */
class MigrateCalEntriesToCalendarCommand extends Command
{
    /**
     * Configure the command by defining the name, options and arguments
     */
    protected function configure(): void
    {
        $this->setDescription('Migrate the content of the retired ext:cal to the calendar_framework.');
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->writeln([
            'Migrate ext:cal entries to calendar_framework',
            '============',
        ]);
        (new MigrateCalToCalendarFoundationService())->processMigration();

        $output->writeln([
            'Done',
            'Please run ./vendor/bin/typo3 calendar:index next.'
        ]);
    }
}

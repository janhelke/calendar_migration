<?php
declare(strict_types=1);

namespace JanHelke\CalendarMigration\Service;

use JanHelke\CalendarMigration\Domain\Repository\CalRepository;
use JanHelke\CalendarMigration\Traits\MigrateCalCalendarsToCalendarFoundationTrait;
use JanHelke\CalendarMigration\Traits\MigrateCalEntriesToCalendarFoundationTrait;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Domain\Repository\PageRepository;
use TYPO3\CMS\Core\Localization\LanguageService;
use TYPO3\CMS\Core\Localization\LanguageServiceFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\StringUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;

/**
 * Class MigrateToCalendarFoundationService
 */
class MigrateCalToCalendarFoundationService
{
    use MigrateCalEntriesToCalendarFoundationTrait;
    use MigrateCalCalendarsToCalendarFoundationTrait;

    protected array $data = [];

    protected array $pidMatching = [];

    /**
     * @return bool
     */
    public function processMigration(): bool
    {
        $this->initializeFakeAdminUser();
        $typo3Version = VersionNumberUtility::convertVersionNumberToInteger(VersionNumberUtility::getCurrentTypo3Version());

        if ($typo3Version < 11_003_000) {
            $GLOBALS['LANG'] = LanguageService::create('default');
        } else {
            $GLOBALS['LANG'] = GeneralUtility::makeInstance(LanguageServiceFactory::class)->create('default');
        }

        $this->data = [
            'pages' => [],
            'tx_calendar_calendar' => [],
            'tx_calendar_entry' => [],
            'tx_calendar_recurrence' => [],
            'tx_calendar_deviation' => [],
            'tx_calendar_exception' => [],
            'tx_calendar_exception_group' => [],
        ];

        $dataHandler = GeneralUtility::makeInstance(DataHandler::class);

        $this->checkAndCreatePages();
        $this->createDataArrayForCalCalendars((new CalRepository())->findAllCalendars());
        $this->createDataArrayForCalEntries((new CalRepository())->findAllEntries());

        $dataHandler->start($this->data, []);
        $ret = $dataHandler->process_datamap();
        return (bool)$ret;
    }

    /**
     * This function serves to check and ensure that we get no exceptions due to
     * missing pages during the migration.
     * Normaly that shouldn't happen, because the new entries will stay on the same
     * pages as the old ones. But we all know the situation having either cold corpses
     * in the database or trying to import a foreign database to a new system.
     */
    protected function checkAndCreatePages(): void
    {
        $pageRepository = GeneralUtility::makeInstance(PageRepository::class);

        $counter = 1;
        foreach ((new CalRepository())->findAllPids() as $pid) {
            if ($pageRepository->checkRecord('pages', $pid)) {
                $this->pidMatching[$pid] = $pid;
            } else {
                $newPageUid = StringUtility::getUniqueId('NEW');
                $this->data['pages'][$newPageUid] = [
                    'pid' => 0,
                    'title' => 'Calendar leftover ' . $counter++,
                    'doktype' => 254
                ];
                $this->pidMatching[$pid] = $newPageUid;
            }
        }
    }

    /**
     * The admin user is required to defined workspace state when working
     * with DataHandler
     */
    protected function initializeFakeAdminUser(): void
    {
        $fakeAdminUser = GeneralUtility::makeInstance(BackendUserAuthentication::class);
        $fakeAdminUser->user = ['uid' => 0, 'username' => '_migration_', 'admin' => 1];
        $fakeAdminUser->id = '0';
        $fakeAdminUser->workspace = 0;
        $GLOBALS['BE_USER'] = $fakeAdminUser;
    }
}

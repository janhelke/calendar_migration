<?php
declare(strict_types=1);

namespace JanHelke\CalendarMigration\Traits;

use DateInterval;
use DateTime;
use DateTimeZone;
use Exception;
use JanHelke\CalendarFoundation\Service\EventService;
use JanHelke\CalendarMigration\Domain\Repository\CalRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\StringUtility;

/**
 * Migrate Cal Entries To Calendar Foundation Service
 */
trait MigrateCalEntriesToCalendarFoundationTrait
{
    protected array $exceptionsMappingArray = [];

    protected array $exceptionGroupsMappingArray = [];

    /**
     * @param array $calEntries
     */
    public function createDataArrayForCalEntries(array $calEntries): void
    {
        $this->migrateAllExceptions();
        $this->migrateExceptionGroups();

        foreach ($calEntries as $calEntry) {
            $newCalendarEntry = $this->migrateEntry($calEntry);
            if ($this->validateCreatedEvent($newCalendarEntry['tx_calendar_entry'])) {
                $newEntryUid = StringUtility::getUniqueId('NEW');
                $this->data['tx_calendar_entry'][$newEntryUid] = $newCalendarEntry['tx_calendar_entry'];
                if (!empty($newCalendarEntry['tx_calendar_recurrence'])) {
                    $this->migrateRecurrences($newCalendarEntry, $newEntryUid, $calEntry);
                }
            }
        }
    }

    /**
     * @param array $calEntry
     * @return array
     */
    public function migrateEntry(array $calEntry): array
    {
        $calenderEntry = $this->initializeEntry($calEntry);
        $calenderEntry['tx_cal_event_uid'] = $calEntry['uid'];

        $recurrenceEntry = [];

        if (!empty($calEntry['freq'])) {
            $recurrenceEntry = $this->calculateRecurrence($calEntry);
        }

        return [
            'tx_calendar_entry' => $calenderEntry,
            'tx_calendar_recurrence' => $recurrenceEntry
        ];
    }

    /**
     * @param string|int $date
     * @param string|int $time
     * @param null $timezone
     * @return string
     * @throws Exception
     */
    protected function convertTime($date, $time, $timezone = null): string
    {
        $dateTime = DateTime::createFromFormat(
            'Ymd',
            (string)$date,
            new DateTimeZone($timezone ?? (new DateTime())->getTimezone()->getName())
        );

        if (($dateTime !== false)) {
            $dateTime->setTime(0, 0, 0);

            if ($time !== 0) {
                $dateTime->setTime(0, 0, 0);
                $dateTime->add(new DateInterval('PT' . $time . 'S'));
            }
        } else {
            $dateTime = new DateTime();
        }

        return $dateTime->format('Y-m-d H:i:s');
    }

    /**
     * @param array $calEntry
     * @return array
     */
    protected function calculateRecurrence(array $calEntry): array
    {
        $recurrenceEntryArray = [];

        switch ($calEntry['freq']) {
            case 'day':
                $this->calculateDailyRecurrence($calEntry, $recurrenceEntryArray);
                break;
            case 'week':
                $this->calculateWeeklyRecurrence($calEntry, $recurrenceEntryArray);
                break;
            case 'month':
                $this->calculateMonthly($calEntry, $recurrenceEntryArray);
                break;
        }

        return $recurrenceEntryArray;
    }

    /**
     * @param array $calEntry
     * @param array $recurrenceEntryArray
     * @throws Exception
     */
    protected function calculateDailyRecurrence(array $calEntry, array &$recurrenceEntryArray): void
    {
        $recurrenceEntry = $this->initializeRecurrenceEntry($calEntry);
        $recurrenceEntry['frequency'] = EventService::DAILY_RECURRENCE;
        $recurrenceEntryArray[] = $recurrenceEntry;
    }

    /**
     * @param array $calEntry
     * @param array $recurrenceEntryArray
     * @throws Exception
     */
    protected function calculateWeeklyRecurrence(array $calEntry, array &$recurrenceEntryArray): void
    {
        $recurrenceEntry = $this->initializeRecurrenceEntry($calEntry);
        $weekArray = [
            'su' => 2 ** 0,
            'mo' => 2 ** 1,
            'tu' => 2 ** 2,
            'we' => 2 ** 3,
            'th' => 2 ** 4,
            'fr' => 2 ** 5,
            'sa' => 2 ** 6
        ];
        $by_day_of_week = 0;
        if (!empty($calEntry['byday'])) {
            foreach (GeneralUtility::trimExplode(',', $calEntry['byday']) as $day) {
                $by_day_of_week += $weekArray[$day];
            }
        }

        $recurrenceEntry['frequency'] = EventService::WEEKLY_RECURRENCE;
        $recurrenceEntry['by_day_of_week'] = $by_day_of_week;
        $recurrenceEntryArray[] = $recurrenceEntry;
    }

    /**
     * @param array $calEntry
     * @param array $recurrenceEntryArray
     * @throws Exception
     */
    protected function calculateMonthly(array $calEntry, array &$recurrenceEntryArray): void
    {
        if (!empty($calEntry['byday'])) {
            $this->calculateMonthlyByIndexAndDay($calEntry, $recurrenceEntryArray);
        } else {
            $this->calculateMonthlyByMonthDay($calEntry, $recurrenceEntryArray);
        }
    }

    /**
     * @param array $calEntry
     * @param array $recurrenceEntryArray
     * @throws Exception
     */
    protected function calculateMonthlyByMonthDay(array $calEntry, array &$recurrenceEntryArray): void
    {
        $recurrenceEntry = $this->initializeRecurrenceEntry($calEntry);
        $monthArray = [];
        for ($i = 1; $i <= 31; ++$i) {
            $monthArray[$i] = 2 ** ($i - 1);
        }

        $by_day_of_month = 0;
        if (!empty($calEntry['bymonthday'])) {
            foreach (GeneralUtility::trimExplode(',', $calEntry['bymonthday']) as $day) {
                $by_day_of_month += $monthArray[$day];
            }
        }

        $recurrenceEntry['frequency'] = EventService::MONTHLY_RECURRENCE;
        $recurrenceEntry['by_day_of_month'] = $by_day_of_month;
        $recurrenceEntryArray[] = $recurrenceEntry;
    }

    /**
     * @param array $calEntry
     * @param array $recurrenceEntryArray
     * @throws Exception
     */
    protected function calculateMonthlyByIndexAndDay(array $calEntry, array &$recurrenceEntryArray): void
    {
        foreach (GeneralUtility::trimExplode(',', $calEntry['byday']) as $byDayAndIndex) {
            $recurrenceEntry = $this->initializeRecurrenceEntry($calEntry);
            if (strlen($byDayAndIndex) === 3) {
                $recurrenceEntry['by_day_index'] = (int)substr($byDayAndIndex, 0, 1);
                $by_day_day = substr($byDayAndIndex, 1, 2);
            } else {
                $recurrenceEntry['by_day_index'] = (int)substr($byDayAndIndex, 0, 2);
                $by_day_day = substr($byDayAndIndex, 2, 2);
            }

            $recurrenceEntry['frequency'] = EventService::MONTHLY_RECURRENCE;
            switch ($by_day_day) {
                case 'su':
                    $recurrenceEntry['by_day_day'] = 1;
                    break;
                case 'mo':
                    $recurrenceEntry['by_day_day'] = 2;
                    break;
                case 'tu':
                    $recurrenceEntry['by_day_day'] = 3;
                    break;
                case 'we':
                    $recurrenceEntry['by_day_day'] = 4;
                    break;
                case 'th':
                    $recurrenceEntry['by_day_day'] = 5;
                    break;
                case 'fr':
                    $recurrenceEntry['by_day_day'] = 6;
                    break;
                case 'sa':
                    $recurrenceEntry['by_day_day'] = 7;
                    break;
            }

            $recurrenceEntryArray[] = $recurrenceEntry;
        }
    }

    /**
     * @param array $calEntry
     * @return array
     * @throws Exception
     */
    protected function initializeRecurrenceEntry(array $calEntry): array
    {
        return [
            'pid' => $this->pidMatching[$calEntry['pid']],
            'tstamp' => $calEntry['tstamp'] ?? 0,
            'crdate' => $calEntry['crdate'] ?? 0,
            'cruser_id' => $calEntry['cruser_id'] ?? 0,
            'frequency' => 0,
            'interval' => $calEntry['intrval'] ?? 1,
            'by_day_of_week' => 0,
            'by_day_index' => 0,
            'by_day_day' => 0,
            'by_day_of_month' => 0,
            'until_date' => empty($calEntry['until']) ? null : $this->convertTime(
                $calEntry['until'],
                0,
                $calEntry['timezone'] ?: 'UTC'
            ),
            'until_recurrence_amount' => $calEntry['cnt'] ?? 0
        ];
    }

    /**
     * This function is only used to inject a pidMatching array for use in tests.
     *
     * @param array $pidMatching
     * @internal
     */
    public function setPidMatching(array $pidMatching): void
    {
        $this->pidMatching = $pidMatching;
    }

    /**
     * @param array $calEntry
     * @return array
     */
    protected function initializeEntry(array $calEntry): array
    {
        return [
            'pid' => $this->pidMatching[$calEntry['pid']],
            'tstamp' => $calEntry['tstamp'] ?? 0,
            'crdate' => $calEntry['crdate'] ?? 0,
            'cruser_id' => $calEntry['cruser_id'] ?? 0,
            'deleted' => 0, //We don't migrate deleted entries
            'hidden' => $calEntry['hidden'] ?? 0,
            'starttime' => $calEntry['starttime'] ?? '',
            'endtime' => $calEntry['endtime'] ?? '',
            'sys_language_uid' => $calEntry['sys_language_uid'] ?? 0,
            'l18n_parent' => $calEntry['l18n_parent'] ?? 0,
            'l10n_state' => $calEntry['l10n_state'] ?? '',
            'l18n_diffsource' => $calEntry['l18n_diffsource'] ?? '',
            'title' => $calEntry['title'] ?? 'no-title',
            'teaser' => $calEntry['teaser'] ?? '',
            'description' => $calEntry['description'] ?? '',
            'start' => empty($calEntry['start_date']) ? '' : $this->convertTime(
                $calEntry['start_date'],
                $calEntry['start_time'] ?? 0
            ),
            'end' => empty($calEntry['end_date']) ? '' : $this->convertTime(
                $calEntry['end_date'],
                $calEntry['end_time'] ?? 0
            ),
        ];
    }

    /**
     * @param array $newCalendarEntry
     * @param string $newEntryUid
     * @param array $calEntry
     * @return mixed
     */
    protected function migrateRecurrences(array $newCalendarEntry, string $newEntryUid, array $calEntry)
    {
        foreach ($newCalendarEntry['tx_calendar_recurrence'] as $recurrence) {
            $recurrenceUid = StringUtility::getUniqueId('NEW');
            $recurrence['entry'] = $newEntryUid;
            $this->data['tx_calendar_recurrence'][$recurrenceUid] = $recurrence;

            if (!empty($calEntry['deviation'])) {
                $this->migrateDeviations($calEntry, $recurrenceUid);
            }

            if (!empty($calEntry['exception_cnt'])) {
                $this->migrateExceptions($calEntry, $recurrenceUid);
            }
        }
    }

    /**
     * @param array $calEntry
     * @param string $recurrenceUid
     */
    protected function migrateDeviations(array $calEntry, string $recurrenceUid): void
    {
        $legacyDeviations = (new CalRepository())->findDeviationsByEventUid((int)$calEntry['uid']);
        if (!empty($legacyDeviations)) {
            $deviationUidArray = [];
            foreach ($legacyDeviations as $legacyDeviation) {
                $deviationUid = StringUtility::getUniqueId('NEW');
                $deviation = $this->initializeEntry($legacyDeviation);
                if ($this->validateCreatedEvent($deviation)) {
                    $deviation['parent_recurrence_uid'] = $recurrenceUid;
                    $deviation['tx_cal_event_deviation_uid'] = $legacyDeviation['uid'];
                    $this->data['tx_calendar_deviation'][$deviationUid] = $deviation;
                    $deviationUidArray[] = $deviationUid;
                }
            }

            $this->data['tx_calendar_recurrence'][$recurrenceUid]['deviation'] = implode(',', $deviationUidArray);
        }
    }

    /**
     * @param array $calEntry
     * @param string $recurrenceUid
     */
    protected function migrateExceptions(array $calEntry, string $recurrenceUid): void
    {
        $legacyExceptions = (new CalRepository())->findExceptionsByEventUid((int)$calEntry['uid']);
        foreach ($legacyExceptions as $legacyException) {
            $this->data['tx_calendar_recurrence'][$recurrenceUid]['exception'][] = $this->exceptionsMappingArray[$legacyException['uid']];
        }
    }

    protected function migrateAllExceptions(): void
    {
        $legacyExceptions = (new CalRepository())->findAllExceptions();
        foreach ($legacyExceptions as $legacyException) {
            $exceptionUid = StringUtility::getUniqueId('NEW');
            $exception = $this->initializeEntry($legacyException);
            if ($this->validateCreatedEvent($exception)) {
                $exception['end'] = DateTime::createFromFormat(
                    'Y-m-d H:i:s',
                    (string)$exception['end'],
                    new DateTimeZone($legacyException['timezone'] ?: 'UTC')
                )
                    ->setTime(23, 59, 59)
                    ->format('Y-m-d H:i:s');
                $exception['tx_cal_exception_event_uid'] = $legacyException['uid'];
                $this->data['tx_calendar_exception'][$exceptionUid] = $exception;
                $this->exceptionsMappingArray[$legacyException['uid']] = $exceptionUid;
            }
        }
    }

    protected function migrateExceptionGroups(): void
    {
        $legacyExceptionGroups = (new CalRepository())->findAllExceptionGroups();
        foreach ($legacyExceptionGroups as $legacyExceptionGroup) {
            $exceptionGroupUid = StringUtility::getUniqueId('NEW');
            $this->data['tx_calendar_exception_group'][$exceptionGroupUid] = [
                'title' => $legacyExceptionGroup['title'],
                'pid' => $this->pidMatching[$legacyExceptionGroup['pid']],
                'tx_cal_exception_event_group_uid' => $legacyExceptionGroup['uid']
            ];
            $legacyExceptions = (new CalRepository())->findAllExceptionsInGroup($legacyExceptionGroup['uid']);
            foreach ($legacyExceptions as $legacyException) {
                $this->data['tx_calendar_exception_group'][$exceptionGroupUid]['exception'][] = $this->exceptionsMappingArray[$legacyException['uid_foreign']];
            }

            $this->exceptionGroupsMappingArray[$legacyExceptions['uid']] = $exceptionGroupUid;
        }
    }

    /**
     * @param array $event
     * @return bool
     */
    protected function validateCreatedEvent(array $event): bool
    {
        return !empty($event['title'])
            && !empty($event['start'])
            && $event['start'] !== '0000-00-00 00:00:00'
            && trim($event['title']) !== '';
    }
}

<?php
declare(strict_types=1);

namespace JanHelke\CalendarMigration\Traits;

use TYPO3\CMS\Core\Utility\StringUtility;

/**
 * Migrate Cal Calendars To Calendar Foundation Service
 */
trait MigrateCalCalendarsToCalendarFoundationTrait
{

    /**
     * @param array $calCalendars
     */
    public function createDataArrayForCalCalendars(array $calCalendars): void
    {
        foreach ($calCalendars as $calCalendar) {
            $this->data['tx_calendar_calendar'][StringUtility::getUniqueId('NEW')] = $this->migrateCalendar($calCalendar);
        }
    }

    /**
     * @param array $calCalendar
     * @return array
     */
    public function migrateCalendar(array $calCalendar): array
    {
        $calenderCalendar = $this->initialzeCalendar($calCalendar);

        switch ($calCalendar['type']) {
            case 0:
                $this->migrateRegularCalendar($calCalendar, $calenderCalendar);
                break;
            case 1:
                $this->migrateICalFeedCalendar($calCalendar, $calenderCalendar);
                break;
            case 2:
                $this->migrateICalFileCalendar($calCalendar, $calenderCalendar);
                break;
            default:
                $this->migrateRegularCalendar($calCalendar, $calenderCalendar);
        }

        if (!empty($calCalendar['activate_fnb'])) {
            $this->migrateFreeAndBusySettings($calCalendar, $calenderCalendar);
            if (!empty($calenderCalendar['fnb_user_cnt'])) {
            }
        }

        return $calenderCalendar;
    }

    /**
     * @param array $calCalendar
     * @param array $calenderCalendar
     */
    protected function migrateRegularCalendar(array $calCalendar, array &$calenderCalendar): void
    {
    }

    /**
     * @param array $calCalendar
     * @param array $calenderCalendar
     */
    protected function migrateICalFeedCalendar(array $calCalendar, array &$calenderCalendar): void
    {
    }

    /**
     * @param array $calCalendar
     * @param array $calenderCalendar
     */
    protected function migrateICalFileCalendar(array $calCalendar, array &$calenderCalendar): void
    {
    }

    /**
     * @param array $calCalendar
     * @param array $calenderCalendar
     */
    protected function migrateFreeAndBusySettings(array $calCalendar, array &$calenderCalendar): void
    {
        $calenderCalendar['activate_free_and_busy'] = 1;

        //substNEWwithIDs
    }

    /**
     * @param array $calCalendar
     * @return array
     */
    protected function initialzeCalendar(array $calCalendar): array
    {
        return [
            'pid' => array_key_exists(
                $calCalendar['pid'],
                $this->pidMatching
            ) ? $this->pidMatching[$calCalendar['pid']] : $calCalendar['pid'],
            'title' => $calCalendar['title'] ?? 'Unnamed calendar',
            'activate_free_and_busy' => 0,
            'free_and_busy_users_and_groups' => 0,
            'type' => $calCalendar['type'] ?: 'calendar',
            'ical_url' => '',
            'ical_file' => 0,
            'refresh_interval' => 0,
            'md5' => '',
            'scheduler_id' => 0,
            'tx_cal_calendar_uid' => $calCalendar['uid'],
        ];
    }
}

<?php
declare(strict_types=1);

namespace JanHelke\CalendarMigration\Update;

use JanHelke\CalendarMigration\Service\MigrateCalToCalendarFoundationService;
use TYPO3\CMS\Install\Updates\AbstractUpdate;

/**
 * Class MigrateCalEntriesToCalendarFoundationLegacyUpdateWizard
 * @deprecated since version 1.0. Will be removed in version 2.0
 * @extensionScannerIgnoreFile
 */
class MigrateToCalendarFoundationLegacyUpdateWizard extends AbstractUpdate
{
    /**
     * @var string
     */
    protected $title = 'Migrate entries from ext:cal to calendar_foundation';

    /**
     * Checks whether updates are required.
     *
     * @param string &$description The description for the update
     * @return bool Whether an update is required (TRUE) or not (FALSE)
     */
    public function checkForUpdate(&$description)
    {
        return true;
    }

    /**
     * Performs the accordant updates.
     *
     * @param array &$dbQueries Queries done in this update
     * @param string &$customMessage Custom message
     * @return bool Whether everything went smoothly or not
     */
    public function performUpdate(array &$dbQueries, &$customMessage): bool
    {
        (new MigrateCalToCalendarFoundationService())->processMigration();
        $this->markWizardAsDone();
        return true;
    }
}

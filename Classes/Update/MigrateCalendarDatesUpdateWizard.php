<?php
declare(strict_types=1);

namespace JanHelke\CalendarMigration\Update;

use DateInterval;
use DateTime;
use DateTimeZone;
use Exception;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Updates\ChattyInterface;
use TYPO3\CMS\Install\Updates\DatabaseUpdatedPrerequisite;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

/**
 * Migrate Calendar Dates Update Wizard
 */
class MigrateCalendarDatesUpdateWizard implements UpgradeWizardInterface, ChattyInterface
{

    /**
     * Setter injection for output into upgrade wizards
     *
     * @param OutputInterface $output
     */
    public function setOutput(OutputInterface $output): void
    {
        // TODO: Implement setOutput() method.
    }

    /**
     * Return the identifier for this wizard
     * This should be the same string as used in the ext_localconf class registration
     *
     * @return string
     */
    public function getIdentifier(): string
    {
        return 'migrateCalendarDatesUpdateWizard';
    }

    /**
     * Return the speaking name of this wizard
     *
     * @return string
     */
    public function getTitle(): string
    {
        return '[ext:cal_backend] Migrate calendar dates';
    }

    /**
     * Return the description for this wizard
     *
     * @return string
     */
    public function getDescription(): string
    {
        return 'Formerly the ext:cal stored start and end of an event separated in days and hours '
            . 'as strings. With this update wizard the existing date is migrated in a timestamp and stored '
            . 'in newly created fields.';
    }

    /**
     * Execute the update
     *
     * Called when a wizard reports that an update is necessary
     *
     * @return bool
     */
    public function executeUpdate(): bool
    {
        return $this->migrateEventDates()
            && $this->migrateExceptionEventDates()
            && $this->migrateDeviationEventDates()
            && $this->migrateEventUntilDates()
            && $this->migrateExceptionEventUntilDates();
    }

    /**
     * Is an update necessary?
     *
     * Is used to determine whether a wizard needs to be run.
     * Check if data for migration exists.
     *
     * @return bool
     */
    public function updateNecessary(): bool
    {
        return $this->checkIfEventsWithOldDatesAndNoNewDatesExists()
            || $this->checkIfExceptionEventsWithOldDatesAndNoNewDatesExists()
            || $this->checkIfDeviationEventsWithOldDatesAndNoNewDatesExists()
            || $this->checkIfEventsWithOldUntilDatesAndNoNewUntilDatesExists()
            || $this->checkIfExceptionEventsWithOldUntilDatesAndNoNewUntilDatesExists();
    }

    /**
     * Returns an array of class names of Prerequisite classes
     *
     * This way a wizard can define dependencies like "database up-to-date" or
     * "reference index updated"
     *
     * @return array<class-string<\TYPO3\CMS\Install\Updates\DatabaseUpdatedPrerequisite>>
     */
    public function getPrerequisites(): array
    {
        return [
            DatabaseUpdatedPrerequisite::class
        ];
    }

    /**
     * @return bool
     */
    protected function checkIfEventsWithOldDatesAndNoNewDatesExists(): bool
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_cal_event');
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $numberOfEntries = $queryBuilder
            ->count('uid')
            ->from('tx_cal_event')
            ->where(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->andX(
                        $queryBuilder->expr()->orX(
                            $queryBuilder->expr()->neq('start_date', 0),
                            $queryBuilder->expr()->neq('start_time', 0)
                        ),
                        $queryBuilder->expr()->eq('start', $queryBuilder->createNamedParameter('0000-00-00 00:00:00'))
                    ),
                    $queryBuilder->expr()->andX(
                        $queryBuilder->expr()->orX(
                            $queryBuilder->expr()->neq('end_date', 0),
                            $queryBuilder->expr()->neq('end_time', 0)
                        ),
                        $queryBuilder->expr()->eq('end', $queryBuilder->createNamedParameter('0000-00-00 00:00:00'))
                    )
                )
            )
            ->execute()
            ->fetchColumn();
        return $numberOfEntries > 0;
    }

    /**
     * @return bool
     * @throws Exception
     */
    protected function migrateEventDates(): bool
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_cal_event');
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $affectedRows = $queryBuilder
            ->select('uid', 'start_date', 'start_time', 'end_date', 'end_time', 'timezone')
            ->from('tx_cal_event')
            ->where(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->andX(
                        $queryBuilder->expr()->orX(
                            $queryBuilder->expr()->neq('start_date', 0),
                            $queryBuilder->expr()->neq('start_time', 0)
                        ),
                        $queryBuilder->expr()->eq('start', $queryBuilder->createNamedParameter('0000-00-00 00:00:00'))
                    ),
                    $queryBuilder->expr()->andX(
                        $queryBuilder->expr()->orX(
                            $queryBuilder->expr()->neq('end_date', 0),
                            $queryBuilder->expr()->neq('end_time', 0)
                        ),
                        $queryBuilder->expr()->eq('end', $queryBuilder->createNamedParameter('0000-00-00 00:00:00'))
                    )
                )
            )
            ->execute()
            ->fetchAll();

        $connection = $connectionPool->getConnectionForTable('tx_cal_event');
        foreach ($affectedRows as $row) {
            $start = DateTime::createFromFormat(
                'Ymd',
                (string)$row['start_date'],
                new DateTimeZone($row['timezone'])
            )->setTime(0, 0, 0);
            $end = DateTime::createFromFormat(
                'Ymd',
                (string)$row['end_date'],
                new DateTimeZone($row['timezone'])
            )->setTime(0, 0, 0);

            if ($start !== false && $end !== false) {
                if ($row['start_time'] !== 0) {
                    $start->add(new DateInterval('PT' . $row['start_time'] . 'S'));
                }

                if ($row['end_time'] !== 0) {
                    $end->add(new DateInterval('PT' . $row['end_time'] . 'S'));
                }

                $connection
                    ->update(
                        'tx_cal_event',
                        [
                            'start' => $start,
                            'end' => $end
                        ],
                        [
                            'uid' => (int)$row['uid']
                        ],
                        [
                            'datetime',
                            'datetime'
                        ]
                    );
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    protected function checkIfEventsWithOldUntilDatesAndNoNewUntilDatesExists(): bool
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_cal_event');
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $numberOfEntries = $queryBuilder
            ->count('uid')
            ->from('tx_cal_event')
            ->where(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->andX(
                        $queryBuilder->expr()->neq('until', 0),
                        $queryBuilder->expr()->eq(
                            'recurring_until',
                            $queryBuilder->createNamedParameter('0000-00-00 00:00:00')
                        )
                    )
                )
            )
            ->execute()
            ->fetchColumn();
        return $numberOfEntries > 0;
    }

    /**
     * @return bool
     * @throws Exception
     */
    protected function migrateEventUntilDates(): bool
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_cal_event');
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $affectedRows = $queryBuilder
            ->select('uid', 'until', 'timezone')
            ->from('tx_cal_event')
            ->where(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->andX(
                        $queryBuilder->expr()->neq('until', 0),
                        $queryBuilder->expr()->eq(
                            'recurring_until',
                            $queryBuilder->createNamedParameter('0000-00-00 00:00:00')
                        )
                    )
                )
            )
            ->execute()
            ->fetchAll();

        $connection = $connectionPool->getConnectionForTable('tx_cal_event');
        foreach ($affectedRows as $row) {
            $recurringUntil = DateTime::createFromFormat(
                'Ymd',
                (string)$row['until'],
                new DateTimeZone($row['timezone'])
            )->setTime(0, 0, 0);

            if ($recurringUntil !== false) {
                $connection
                    ->update(
                        'tx_cal_event',
                        [
                            'recurring_until' => $recurringUntil
                        ],
                        [
                            'uid' => (int)$row['uid']
                        ],
                        [
                            'datetime'
                        ]
                    );
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    protected function checkIfExceptionEventsWithOldDatesAndNoNewDatesExists(): bool
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_cal_exception_event');
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $numberOfEntries = $queryBuilder
            ->count('uid')
            ->from('tx_cal_exception_event')
            ->where(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->andX(
                        $queryBuilder->expr()->neq('start_date', 0),
                        $queryBuilder->expr()->eq('start', $queryBuilder->createNamedParameter('0000-00-00 00:00:00'))
                    ),
                    $queryBuilder->expr()->andX(
                        $queryBuilder->expr()->neq('end_date', 0),
                        $queryBuilder->expr()->eq('end', $queryBuilder->createNamedParameter('0000-00-00 00:00:00'))
                    )
                )
            )
            ->execute()
            ->fetchColumn();
        return $numberOfEntries > 0;
    }

    /**
     * @return bool
     * @throws Exception
     */
    protected function migrateExceptionEventDates(): bool
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_cal_exception_event');
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $affectedRows = $queryBuilder
            ->select('uid', 'start_date', 'end_date', 'timezone')
            ->from('tx_cal_exception_event')
            ->where(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->andX(
                        $queryBuilder->expr()->neq('start_date', 0),
                        $queryBuilder->expr()->eq('start', $queryBuilder->createNamedParameter('0000-00-00 00:00:00'))
                    ),
                    $queryBuilder->expr()->andX(
                        $queryBuilder->expr()->neq('end_date', 0),
                        $queryBuilder->expr()->eq('end', $queryBuilder->createNamedParameter('0000-00-00 00:00:00'))
                    )
                )
            )
            ->execute()
            ->fetchAll();

        $connection = $connectionPool->getConnectionForTable('tx_cal_exception_event');
        foreach ($affectedRows as $row) {
            $start = DateTime::createFromFormat(
                'Ymd',
                (string)$row['start_date'],
                new DateTimeZone($row['timezone'])
            )->setTime(0, 0, 0);
            $end = DateTime::createFromFormat(
                'Ymd',
                (string)$row['end_date'],
                new DateTimeZone($row['timezone'])
            )->setTime(0, 0, 0);

            if ($start !== false && $end !== false) {
                $connection
                    ->update(
                        'tx_cal_exception_event',
                        [
                            'start' => $start,
                            'end' => $end
                        ],
                        [
                            'uid' => (int)$row['uid']
                        ],
                        [
                            'datetime',
                            'datetime'
                        ]
                    );
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    protected function checkIfExceptionEventsWithOldUntilDatesAndNoNewUntilDatesExists(): bool
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_cal_exception_event');
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $numberOfEntries = $queryBuilder
            ->count('uid')
            ->from('tx_cal_exception_event')
            ->where(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->andX(
                        $queryBuilder->expr()->neq('until', 0),
                        $queryBuilder->expr()->eq(
                            'recurring_until',
                            $queryBuilder->createNamedParameter('0000-00-00 00:00:00')
                        )
                    )
                )
            )
            ->execute()
            ->fetchColumn();
        return $numberOfEntries > 0;
    }

    /**
     * @return bool
     * @throws Exception
     */
    protected function migrateExceptionEventUntilDates(): bool
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_cal_exception_event');
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $affectedRows = $queryBuilder
            ->select('uid', 'until', 'timezone')
            ->from('tx_cal_exception_event')
            ->where(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->andX(
                        $queryBuilder->expr()->neq('until', 0),
                        $queryBuilder->expr()->eq(
                            'recurring_until',
                            $queryBuilder->createNamedParameter('0000-00-00 00:00:00')
                        )
                    )
                )
            )
            ->execute()
            ->fetchAll();

        $connection = $connectionPool->getConnectionForTable('tx_cal_exception_event');
        foreach ($affectedRows as $row) {
            $recurringUntil = DateTime::createFromFormat(
                'Ymd',
                (string)$row['until'],
                new DateTimeZone($row['timezone'])
            )->setTime(0, 0, 0);

            if ($recurringUntil !== false) {
                $connection
                    ->update(
                        'tx_cal_exception_event',
                        [
                            'recurring_until' => $recurringUntil
                        ],
                        [
                            'uid' => (int)$row['uid']
                        ],
                        [
                            'datetime'
                        ]
                    );
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    protected function checkIfDeviationEventsWithOldDatesAndNoNewDatesExists(): bool
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_cal_event_deviation');
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $numberOfEntries = $queryBuilder
            ->count('uid')
            ->from('tx_cal_event_deviation')
            ->where(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->andX(
                        $queryBuilder->expr()->orX(
                            $queryBuilder->expr()->neq('orig_start_date', 0),
                            $queryBuilder->expr()->neq('orig_start_time', 0)
                        ),
                        $queryBuilder->expr()->eq(
                            'original_start',
                            $queryBuilder->createNamedParameter('0000-00-00 00:00:00')
                        )
                    ),
                    $queryBuilder->expr()->andX(
                        $queryBuilder->expr()->orX(
                            $queryBuilder->expr()->neq('start_date', 0),
                            $queryBuilder->expr()->neq('start_time', 0)
                        ),
                        $queryBuilder->expr()->eq('start', $queryBuilder->createNamedParameter('0000-00-00 00:00:00'))
                    ),
                    $queryBuilder->expr()->andX(
                        $queryBuilder->expr()->orX(
                            $queryBuilder->expr()->neq('end_date', 0),
                            $queryBuilder->expr()->neq('end_time', 0)
                        ),
                        $queryBuilder->expr()->eq('end', $queryBuilder->createNamedParameter('0000-00-00 00:00:00'))
                    )
                )
            )
            ->execute()
            ->fetchColumn();
        return $numberOfEntries > 0;
    }

    /**
     * @return bool
     * @throws Exception
     */
    protected function migrateDeviationEventDates(): bool
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_cal_event_deviation');
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $affectedRows = $queryBuilder
            ->select(
                'uid',
                'orig_start_date',
                'orig_start_time',
                'start_date',
                'start_time',
                'end_date',
                'end_time',
                'timezone'
            )
            ->from('tx_cal_event_deviation')
            ->where(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->andX(
                        $queryBuilder->expr()->orX(
                            $queryBuilder->expr()->neq('orig_start_date', 0),
                            $queryBuilder->expr()->neq('orig_start_time', 0)
                        ),
                        $queryBuilder->expr()->eq(
                            'original_start',
                            $queryBuilder->createNamedParameter('0000-00-00 00:00:00')
                        )
                    ),
                    $queryBuilder->expr()->andX(
                        $queryBuilder->expr()->orX(
                            $queryBuilder->expr()->neq('start_date', 0),
                            $queryBuilder->expr()->neq('start_time', 0)
                        ),
                        $queryBuilder->expr()->eq('start', $queryBuilder->createNamedParameter('0000-00-00 00:00:00'))
                    ),
                    $queryBuilder->expr()->andX(
                        $queryBuilder->expr()->orX(
                            $queryBuilder->expr()->neq('end_date', 0),
                            $queryBuilder->expr()->neq('end_time', 0)
                        ),
                        $queryBuilder->expr()->eq('end', $queryBuilder->createNamedParameter('0000-00-00 00:00:00'))
                    )
                )
            )
            ->execute()
            ->fetchAll();

        $connection = $connectionPool->getConnectionForTable('tx_cal_event_deviation');
        foreach ($affectedRows as $row) {
            $originalStart = DateTime::createFromFormat(
                'Ymd',
                (string)$row['orig_start_date'],
                new DateTimeZone($row['timezone'])
            )->setTime(0, 0, 0);
            $start = DateTime::createFromFormat(
                'Ymd',
                (string)$row['start_date'],
                new DateTimeZone($row['timezone'])
            )->setTime(0, 0, 0);
            $end = DateTime::createFromFormat(
                'Ymd',
                (string)$row['end_date'],
                new DateTimeZone($row['timezone'])
            )->setTime(0, 0, 0);

            if ($originalStart !== false && $start !== false && $end !== false) {
                if ($row['orig_start_time'] !== 0) {
                    $originalStart->add(new DateInterval('PT' . $row['orig_start_time'] . 'S'));
                }

                if ($row['start_time'] !== 0) {
                    $start->add(new DateInterval('PT' . $row['start_time'] . 'S'));
                }

                if ($row['end_time'] !== 0) {
                    $end->add(new DateInterval('PT' . $row['end_time'] . 'S'));
                }

                $connection
                    ->update(
                        'tx_cal_event_deviation',
                        [
                            'original_start' => $originalStart,
                            'start' => $start,
                            'end' => $end
                        ],
                        [
                            'uid' => (int)$row['uid']
                        ],
                        [
                            'datetime',
                            'datetime',
                            'datetime'
                        ]
                    );
            }
        }

        return true;
    }
}
